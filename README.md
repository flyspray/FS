This project is a dummy project with issues imported from our
old Flyspray bug tracker.

You can continue discussion here and reference bugs from all other GitLab
repositories by writing:

```
flyspray/FS#xyz
```

**Please do not open any new bugs in the flyspray project**,
but instead file them under the correct core module.